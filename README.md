#插件名称: JSPinyin
#JSPinyin有提供了两个方法：
#1）一个是将汉字翻译为拼音，其中每一个字的首字母大写；
#pinyin.getFullChars("汉字");
#2）一个是可以将每一个字的拼音的首字母提取出来，是大写的形式。
#pinyin.getCamelChars("汉字");
#JSPinyin依赖于mootools这个JavaScript库，可以自行修改为prototype库或其他。
#<script type=”text/javascript” src=”mootools-core-1.3.2-full-compat.js”></script>
#<script type=”text/javascript” src=”pinyin.js”></script>
#<script type=”text/javascript”>
#var pinyin = new Pinyin();
#alert(pinyin.getFullChars(“我是小写的拼音”).toLowerCase());
#</script>
#需要注意下的是 先调用mootools库在调用JSPinyin!
#mootools 库没有的可以在GOOGLE下载


#PHP插件参考网址 http://overtrue.me/articles/2014/10/php-chinese-to-pinyin.html